# VNC Tutorial
How to use the remote desktop on SCIPP servers. They run [Xtightvnc](https://www.tightvnc.com/) due to its accessibility on Ubuntu Linux distrubutions. This tutorial will assume the Xtightvnc is installed on the machine since IT department will manage the Linux system operation.

# Prerequisite Check Up

## Terminal Program
Windows ships with a primitive terminal program. But you can install a much fancier terminal program, developed none other than the Microsoft themselves, from Microsoft Store: <b>Windows Terminal</b>. Simply run the Microsoft Store from the Start then install it from there. 

```Start --> Microsoft Store --> Search for Windows Terminal --> Install it```

Or you can follow [this](https://www.microsoft.com/p/windows-terminal/9n0dx20hk701) link to the Microsoft Store page.

## OpenSSH
You need to be able to connect to the server via [secure shell](https://en.wikipedia.org/wiki/Secure_Shell)(ssh). There are many ssh implementations but '[OpenSSH](https://www.openssh.com/)' is adopted by many users. In fact, they are already shipped with many non-Windows OS' such as Mac OS and most of Linux desktop distributions. But on Windows, you have to take some steps to install them as indicated [here](https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse?tabs=gui).

* On Windows 10, ```Windows Button --> Settings --> Optional Features``` then find out if you have OpenSSH client is installed. If the checkbox is not highlighted, click it to enable it. You do not need to install OpenSSH server to connect to SCIPP servers.
* On Windows 11, ```Windows Button --> Settings --> System --> Optional features --> Add an optional feature --> Click [View features]``` Then search OpenSSH Client to install it.

Here are some figures to guide the navigation.
![Windows 11 Openssh 1](./imgs/windows_openssh1.png)
![Windows 11 Openssh 2](./imgs/windows_openssh2.png)
![Windows 11 Openssh 3](./imgs/windows_openssh3.png)

## PuTTY (Optional)
Alternatively, you can install [PuTTY](https://www.putty.org/) to install OpenSSH environment. You can take this route if you cannot change Windows features due to facility security reasons etc. Some national labs and companies, i.e. Samsung, prohibit users to change system settings. If all fails, you better ask system admins or IT departments to install those software.

## RealVNC Viewer
To view any VNC desktop, you need to install a specialized VNC desktop viewer. There are many of them on the market and some of SSH client programs like `[MOBAXTerm](https://mobaxterm.mobatek.net/)' ships with it. Another recommended VNC viewer is '[RealVNC Viewer](https://www.realvnc.com/en/connect/download/viewer/)' provided by RealVNC. They charge some hefty amount of cash to use their 'Cloud' based VNC service but Viewer itself is free, at least as of June of 2024. Also, the RealVNC Viewer ships with Raspbian OS which is an 'official' OS for Raspberry Pi.

RealVNC Viewer
![RealVNC Viewer](./imgs/realvnc_viewer.png)

## University Campus VPN
Most likely, you will be connecting to the SCIPP servers from out of the University networks. University Campus VPN is a must, obviously. 

## Assign Your Ports
SCIPP TightVNC assigns network ports per eash person. So, if multiple people using it, they need to be assigned to a specific port number. Most likely, network port 5900 to 5910 will be opened per server. But it can be managed differently from server to server. Ask server administrator for VNC port assignments. 

# Connecting to VNC Server
## Connect to the Campus VPN

Obviously, connect to VPN if you are outside of the campus network. Otherwise, nothing will work out.

## Connect to the server using ssh

1. Open up the Windows Terminal then type ssh connection command below. Say, your server ID is 'johndoe' and the server address is 'some-ufsd-server.ucsc.edu'
    ```
    ssh johndoe@some-ufsd-server.ucsc.edu
    ```

2. If you have not set your own password, use `passwd` command to change your account password for your own sake.

    ```
    passwd
    ```

You will have to type current, given by system admin, password first then type your own password twice to change it.

3. Now check up if you can run VNC server for your own account.

    ```
    which vncserver
    ```
    If anything comes up with this command, you will have to ask system admin to install VNC server program on the server. This ```which``` command shows the program's location if you can run the program. If thic command returns something like ```/usr/bin/vncserver``` you are OK.      

4. Next, check up if you can run `xfce4` or `lxde` desktop environment on the server.   
    ```
    which startxfce4
    ```
    or
    ```
    which startlxde
    ```
    If nothing returns from both of commands, you will need to ask system admin to install desktop environments for Tightvnc. Preferrably `xfce4` or `lxde`. Ubuntu adopts `Gnome` desktop environment by default but for some reason, it does not work well with TightVNC. If you are interested, you can find some information on those desktops: 
    
    * [Xfce4](https://xfce.org/)
    * [LXDE](https://www.lxde.org/)
    
    They are 'lightweight' desktop environments to save system resources and network overhead in remote desktop sessions.
    
5. (Optional) Set up VNC password for your own VNC session.
    ```
    vncpasswd
    ```
    Will prompt you to take a separate passord dedicated to your own VNC session. You can also make a `view only` password separately as well.

6. Now, we are ready to run the actual VNC server. If your assigned network port on the server is ```5903```, type like this:

    ```
    vncserver :3
    ```
    Then the server will run and generate a few setting files for your own VNC server session. However, you are not ready. You will need to manually set up the desktop environment for your own VNC session. 

    First, we should kill the VNC session we just created:
    ```
    vncserver -kill :3
    ```
    
    To set up the desktop environment, you have to edit a file in your own directory.

    ```
    nano ~/.vnc/xstartup
    ```
    Then, in the file, add followings:
    ```
    xrdb $HOME/.Xresources
    startxfce4 & 
    ```
    Change `startxfce4` to `startlxde` if the server has lxde as VNC desktop environment.

7. Now, actually run a VNC server session for yourself.
    ```
    vncserver :3 -geometry 1600x900
    ```
    There, the `-geometry 1600x900` will be your remote desktop resolution. If this option is not given, it will be `1024x768` resolution or smaller: not very good for GUI heavy CAD tools. Set it up as much as possible to fit your need.

    As long as the server does not reboot or someone kills your VNC server session, the session will last even if you disconnect from the server. 


## Connect VNC server from Host
Obviously, Run any VNC viewer program you have installed on your host PC  and type in the server address (some-ufsd-server.ucsc.edu for this example) for VNC session, without your ID, to the textbox look like below:
![RealVNC connect addr](./imgs/realvnc_server_addr.png)

Then an authentication window will pop up:
![RealVNC connect auth](./imgs/realvnc_auth.png)

You can type in <b>VNC Password</b>, NOT your server password here. If you have not set up VNC password, leave the password blank. Also, if the program warns lack of encryption, just ignore the warning. The TightVNC server does not support encryption with current server setting.


Now you can finally see your VNC desktop!!
![VNCDesktopExample](./imgs/realvnc_vnc_desktop.png)
    
From now on, as long as the VNC server is running on the SCIPP server, you can connect to the desktop session anytime. You will need to run the server if the server comes back from maintenance reboot, though.