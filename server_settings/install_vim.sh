#!/bin/bash

set -e

#
# Vim installation - to local dir - script
#

NCPU="$(nproc --all)"
WORK_DIR="${HOME}/.vim_install"
PREFIX="${HOME}/.homebrew"
VIM_VER="9.1.1106"
VIM_SRC_LINK="https://github.com/vim/vim/archive/v9.1.1106/vim-${VIM_VER}.tar.gz"

NCURSES_VER='6.4'
NCURSES_LINK="https://github.com/mirror/ncurses/archive/refs/tags/v${NCURSES_VER}.tar.gz"

# Checking if the custom (or newer) vim is already installed...
#
[ -x "$(command -v $PREFIX/bin/vim)" ] && \
  printf 'custom vim already installed!\n' && \
  exit 0;


if [ -d "${WORK_DIR}" ]; then
  rm -rf "${WORK_DIR}"
fi
printf 'Making a work directory: %s\n' "${WORK_DIR}"
mkdir -p "${WORK_DIR}"

if [ ! -d "${PREFIX}" ]; then
  printf '%s prefix dir not found!\n' "${PREFIX}"
  return 1
fi

pushd "${WORK_DIR}"

# Installing ncurses if it doesn't exist in the system
#
if [ -z "$(ls $PREFIX/lib | grep libncurses)" ]; then
  wget "${NCURSES_LINK}" -O "./libncurses-${NCURSES_VER}.tar.gz"
  tar xvf "./libncurses-${NCURSES_VER}.tar.gz"
  pushd "./ncurses-${NCURSES_VER}"

  ./configure --prefix="${PREFIX}" \
    --with-shared \
    --without-debug \
    --without-normal \
    --with-cxx-shared \
    --enable-pc-files \
    --with-pkg-config-libdir="${PREFIX}/lib/pkgconfig" \
    && \
      make -j ${NCPU} && make install

  popd
fi

wget "${VIM_SRC_LINK}"
tar xvf "vim-${VIM_VER}.tar.gz"

pushd ./vim-${VIM_VER}
LDFLAGS="-L${PREFIX}/lib" ./configure --prefix="${PREFIX}" \
  --with-features=huge && \
  make -j${NCPU} && make install

popd
popd

# Removing work directory
rm -rf "${WORK_DIR}"
printf 'Jobs Finished\n'
