#!/bin/sh
set -e

#
# A script to install settings and local programs (without sudo access) for UCSC servers.
#
# --> Coded for ufsdpower.ucsc.edu
#     But can be implemented to anything runs Ubuntu 22.04 as a main OS.
#
# Prerequisites:
# git, vim, libz1ib-dev
#

# Logo Screen
printf '**** Setting up environment for UCSC servers ****\n'

# Directories
USR_DIR="${HOME}"
SETTINGS_DIR="${HOME}/.settings"
HOMEBREW="${HOME}/.homebrew"

# Git Repo (settings)
MY_GIT_REPO='git@github.com:Taris9047/taris-personal-settings.git'
MY_UDS_GIT_REPO='git@github.com:Taris9047/uds.git'

printf 'Target directory: %s\n' "${USR_DIR}"
printf 'Source directory: %s\n' "${SETTINGS_DIR}"

# Check git.
[ ! -x "$(command -v git)" ] && printf 'ERROR! No git found!!\n' && exit 1

# Managing Directories
# Cloning the main settings...
[ ! -d "$SETTINGS_DIR" ] && git clone "$MY_GIT_REPO" "$SETTINGS_DIR"
{ [ -d "$SETTINGS_DIR" ] && [ ! -L "$HOME/Settings" ]; } && { ln -sfv "$SETTINGS_DIR" "$HOME/Settings" || true; }

# Make homebrew dir
[ ! -d "$HOMEBREW" ] && mkdir -pv "${HOMEBREW}"
{ [ -d "$HOMEBREW" ] && [ ! -L "$HOME/Homebrew" ]; } && { ln -sfv "$HOMEBREW" "$HOME/Homebrew" || true; }

#
# Some Array Utilities for POSIX shells
#
# Implementing a poor man's array in POSIX way.
# referenced: https://github.com/krebs/array/blob/master/array
#
# Array constructor
array() {
  for i in "$@"; do
    printf '%s\n' "$i" | array_element_encode
  done
}

# Array element eocode/decode
array_element_encode() {
  sed 's/%/%25/g' | sed -e :a -e '$!N; s/\n/%0A/; ta'
}

array_element_decode() {
  sed -e 's/%0[aA]/\
/g' -e 's/%25/%/g'
}

#
# Append string to file...
#
append_string() {
  if ! grep -Fxq "${2}" "${1}"; then
    # printf 'Appending %s with %s\n' "${2}" "${1}"
    printf "%s\n" "${2}" >>"${1}"
  fi
}
append_source() {
  append_string "${1}" ". ${2}"
}

# Installing environments from .settings dir.
printf 'Setting up shell environments\n'
# Importing bash settings
DOTFILESDIR="$SETTINGS_DIR/dotfiles"
LINUXBASHFILE="$DOTFILESDIR/bashrc_linux"
LINUXZSHFILE="$DOTFILESDIR/zshrc_linux"
DARWINBASHFILE="$DOTFILESDIR/bash_profile_osx"
DARWINZSHFILE="$DOTFILESDIR/zshrc_osx"
# SHELL_TYPE="$(echo $SHELL)"

inst_env_linux() {
  [ ! -f "$HOME/.bashrc" ] && touch "$HOME/.bashrc"
  append_source "$HOME/.bashrc" "$LINUXBASHFILE"

  [ ! -f "$HOME/.zshrc" ] && touch "$HOME/.zshrc"
  append_source "$HOME/.zshrc" "$LINUXZSHFILE"
}

# Yes, it is always linux!
inst_env_linux

# Config Files - This part needs to be POSIXified!
CONF_LIST=$(array 'vimrc' 'gitignore' 'gitconfig' 'gdbinit' 'Xresources' 'tmux.conf' 'profile')

# Linking dotfiles
printf "\nLinking dotfiles from settings dir ...\n"
# Iterate with CONF_LIST
#
# Do this stuff for each element
do_inst() {
  printf 'Installing: %s\n' ".${1}"
  rm -rf "${USR_DIR}/.${1}"
  ln -sf "${SETTINGS_DIR}/dotfiles/${1}" "${USR_DIR}/.${1}" || true
}
# Iterate through the array...
printf '%s\n' "$CONF_LIST" |
  while IFS= read -r element; do
    do_inst "$(printf '%s\n' "$element" | array_element_decode)"
  done

# Still linking dotfiles... but to some odd locations.
# i.e. .config or .local/config directories etc.
printf '\nWorking on other config files ...\n'
# VIM -- MacOS comes with VIM. Thus installing it.
printf 'Setting up VIM package directory\n'
VIM_CONF_DIR="${USR_DIR}/.vim"
VIM_SETTINGS_DIR="${SETTINGS_DIR}/dotfiles/vim"
[ ! -d "${VIM_CONF_DIR}" ] && rm -rf "$VIM_CONF_DIR"
mkdir -pv "${VIM_CONF_DIR}"
# ln -sf "$VIM_SETTINGS_DIR/autoload" "$VIM_CONF_DIR/autoload"
ln -sf "${VIM_SETTINGS_DIR}/colors" "${VIM_CONF_DIR}/colors"
mkdir -p "${VIM_CONF_DIR}/pack"
mkdir -p "${VIM_CONF_DIR}/terminal_colors"

# NVIM - or Neovim
printf 'Setting up NVIM config file\n'
NVIM_CONF_HOME="${USR_DIR}/.config/nvim"
NVIM_GTK_CONF_HOME="$USR_DIR/.config/nvim-gtk"
#[ ! -d "$NVIM_CONF_HOME" ] && mkdir -pv "$NVIM_CONF_HOME"
#[ ! -d "$NVIM_GTK_CONF_HOME" ] && mkdir -pv "$NVIM_GTK_CONF_HOME"
#rm -rf "$NVIM_CONF_HOME/*init.vim"
#ln -sf "$SETTINGS_DIR/dotfiles/init.vim.nvim" "$NVIM_CONF_HOME/init.vim" || true
#ln -sf "$SETTINGS_DIR/dotfiles/init.vim.nvim" "$NVIM_CONF_HOME/sysinit.vim" || true
#ln -sf "$SETTINGS_DIR/dotfiles/ginit.vim.nvim" "$NVIM_CONF_HOME/ginit.vim" || true
#
# Now installs NvChad instead of copying VIM's setting directly.
#
rm -rf "${NVIM_CONF_HOME}" "${NVIM_GTK_CONF_HOME}"
if [ -d "${USR_DIR}/.local/share/nvim" ]; then
  find "${USR_DIR}/.local/share/nvim/" -maxdepth 1 ! -name "nvim" -prune -name "runtime" -type d -exec rm -rf {} +
fi
#rm -rf "${USR_DIR}/.local/share/nvim"
git clone "https://github.com/NvChad/NvChad" "${HOME}/.config/nvim" --depth 1 

# Starship
printf 'Setting up starship config file\n'
STARSHIP_CONF_FILE="${USR_DIR}/.config/starship.toml"
[ ! -f "${STARSHIP_CONF_FILE}" ] && (ln -sfv "${SETTINGS_DIR}/dotfiles/starship.toml" "${STARSHIP_CONF_FILE}" || true)

# Alacritty - an OpenGL based terminal
printf 'Setting up Alacritty config file\n'
ALACRITTY_CONF_FILE="${USR_DIR}/.config/alacritty.yml"
ALACRITTY_CONF_FILE_TOML="${USR_DIR}/.config/alacritty.toml"
rm -rf "${ALACRITTY_CONF_FILE}" "${ALACRITTY_CONF_FILE_TOML}"
ln -sf "${SETTINGS_DIR}/dotfiles/alacritty.toml" "${ALACRITTY_CONF_FILE_TOML}" || true


# Installing Rust
#
# Rust tool list
# --> Note that some rust tools cannot be compiled on MacOS due to
# lack of convenient openssl library link.
# We need to handle it for some way.
#
RUST_LIST=$(array 'exa' 'bat' 'rm-improved' 'diskonaut' 'ripgrep' 'fd-find' 'tokei' 'lsd' 'procs' 'hjson' 'eza' 'du-dust')

# Rust tool install
do_rust_inst () {
  CMD_TO_INST="${1}"
  CARGO_BIN="$(command -v cargo)"
  if [ "${1}" = "fd-find" ]; then
    CMD_TO_INST="fd"
  elif [ "${1}" = "ripgrep" ]; then
    CMD_TO_INST="rg"
  elif [ "${1}" = "rm-improved" ]; then
    CMD_TO_INST="rip"
  fi

  if [ -z "$(command -v ${CMD_TO_INST})" ]; then
    "${CARGO_BIN}" install "${1}"
  fi
}

# Install Cargo
if [ -z "$(command -v cargo)" ]; then
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
fi

printf 'If the script halts after installing cargo, please run . ~/.bashrc\nThen run the script again...\n'

# Install Rust tools
printf '%s\n' "${RUST_LIST}" |
  while IFS= read -r element; do
    [ ! -x "$(command -v { "$element" | array_element_decode } )" ] && \
    do_rust_inst "$(printf '%s\n' "$element" | array_element_decode)"
  done

# Installing Starship
if [ -z "$(command -v starship)" ]; then
  CARGO_BIN="$(command -v cargo)"
  "${CARGO_BIN}" install starship --locked
  # curl -sS https://starship.rs/install.sh | sh
fi

#
# Node.JS
#
if [ ! -d "${HOME}/.nvm" ]; then
  # Install command from 'nodejs.org/en/download' as of Feb. 2025
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.1/install.sh | bash && \
  . "${HOME}/.nvm/nvm.sh"
  nvm install 22 && \
  printf 'node+npm installation finished!\n'
fi

# Setting up NVChad - Needs Lua
if [ -x "$(command -v nvim)" ]; then
  printf 'Setting up NVIM config files for NvChad\n'
  NVIM_CONF_HOME="${USR_DIR}/.config/nvim"
  NVIM_GTK_CONF_HOME="$USR_DIR/.config/nvim-gtk"
  rm -rf "${NVIM_CONF_HOME}" "${NVIM_GTK_CONF_HOME}"
  find "${USR_DIR}/.local/share/nvim/" -maxdepth 1 ! -wholename "${USR_DIR}/.local/share/nvim/" -prune -wholename "runtime" -type d -exec rm -rf {} +
  #rm -rf "${USR_DIR}/.local/share/nvim"
  git clone "https://github.com/NvChad/starter" "${HOME}/.config/nvim" --depth 1 
fi

# Tmux!
printf 'Setting up Tmux Package Manager (TPM)\n'
[ ! -d "$HOME/.tmux/plugins/tpm" ] && git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

printf '\n'
printf '\n**** Closing Comments ****\n'

if [ -z "${HOME}/.gitconfig.local" ]; then
  printf 'TODO: Also, don''t forget to populate %s\n\n' "$HOME/.gitconfig.local"
fi

if [ -x "$(command -v tmux)" ]; then
  printf 'Sometimes tmux does not run install packages automatically: Ctrl+B Shift+I will do it manually.\n'
fi

# Setting up Ansys HFSS
#
ANSYS_ROOT='/opt/AnsysEM'
HFSS_ARCH='Linux64'
HFSS_VER='v241'
HFSS_DIR="${ANSYS_ROOT}/${HFSS_VER}/${HFSS_ARCH}"

if [ -d "${ANSYS_ROOT}" ]; then
  printf 'AnsysEM Root directory found!!\n'
  if [ -d "${ANSYS_ROOT}/${HFSS_VER}" ]; then
    printf 'Version %s found!!\n' "${HFSS_VER}"
    printf 'Expanding PATH\n'
    append_string \
      "$HOME/.bashrc" '# HFSS Setup'
    append_string \
      "$HOME/.bashrc" "HFSS_DIR=\"${HFSS_DIR}\""
    append_string \
      "$HOME/.bashrc" 'export PATH="${HFSS_DIR}:${PATH}"'
    append_string \
      "$HOME/.bashrc" 'echo ''Ansys HFSS Environment enabled!! Type ansysedt to run HFSS!!'
  
  fi
fi




printf 'Have a nice day!\n\n'
