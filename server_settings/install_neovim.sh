#!/bin/bash

#
# Let's install neovim to the system.
#
# --> NvChad is the best!!
#

CWD=dir="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)"

NVCHAD_GOOD_VER='v0.10.4'
printf 'Current NvChad Recommendation: %s\n' "${NVCHAD_GOOD_VER}"

# Installation prefix
PREFIX="${HOME}/.homebrew"


# If neovim is installed, quit...
[ -x "$(command -v $PREFIX/bin/nvim)" ] && \
  printf 'Good Neovim seems to be already installed!\n' && \
  "${PREFIX}/bin/nvim" --version && \
  exit 0


# Making work directory...
WORK_DIR="${HOME}/.neovim_install"
if [ ! -d "${WORK_DIR}" ]; then
  printf 'Making work directory: %s\n' "${WORK_DIR}" 
  mkdir -p "${WORK_DIR}"
fi

# Checking up lua. If doesn't exist, install it...
if [ -z "$(command -v lua)" ]; then
  LUA_VER='5.4.7'
  LUA_VER_MID='5.4'
  pushd "${WORK_DIR}" 
  wget "https://www.lua.org/ftp/lua-${LUA_VER}.tar.gz" -O ./lua-${LUA_VER}.tar.gz 
  tar xvf ./lua-${LUA_VER}.tar.gz 
  pushd ./lua-${LUA_VER} 

  # pkg-config patching...
  cat > lua.pc << "EOF"
V=$LUA_VER_MID
R=$LUA_VER

prefix=/usr
INSTALL_BIN=${prefix}/bin
INSTALL_INC=${prefix}/include
INSTALL_LIB=${prefix}/lib
INSTALL_MAN=${prefix}/share/man/man1
INSTALL_LMOD=${prefix}/share/lua/${V}
INSTALL_CMOD=${prefix}/lib/lua/${V}
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${R}
Requires:
Libs: -L${libdir} -llua -lm -ldl
Cflags: -I${includedir}
EOF

  wget "https://www.linuxfromscratch.org/patches/blfs/svn/lua-5.4.7-shared_library-1.patch"
  patch -Np1 -i "./lua-${LUA_VER}-shared_library-1.patch" && \
    make linux

  make INSTALL_TOP="${PREFIX}" \
       INSTALL_DATA="cp -d"\
       INSTALL_MAN="${PREFIX}/share/man/man1" \
       TO_LIB="liblua.so liblua.so.${LUA_VER_MID} liblua.so.${LUA_VER}" \
       install && \
  mkdir -pv "${PREFIX}/share/doc/lua-${LUA_VER}" && \
  cp -v doc/*.{html,css,giv,png} "${PREFIX}/share/doc/lua-${LUA_VER}" && \
  install -v m644 lua.pc "${PREFIX}/lib/pkgconfig/lua.pc"

  popd
  popd

fi

#
# Checking if nvim is already installed!!
#
if [ -x "$(command -v nvim)" ]; then
  NVIM_VERSION=`nvim --version | awk -F' ' 'NR==1{print $2}'`
  printf 'Neovim %s found in the path!!\n' "${NVIM_VERSION}"
  if [ "${NVIM_VERSION}" = "${NVCHAD_GOOD_VER}" ]; then
    printf 'Guess what? we have a good Neovim for NVCHAD!!\n'
    if [ -x "$(command -v lua)" ]; then
      printf 'We also have lua!!\nYou can install Neovim now!!\n'
    else
      printf 'All that we are missing is lua!!\n'
    fi
  else
    printf 'Not a good Neovim Version... Please re-install correct one.\n'
  fi
  printf 'Exiting without doing anyting... \n'
  exit 0
fi

#
# Check up if we have all the programs...
#
[ ! -x "$(command -v git)" ] && printf 'Error: we need git!!\n' && exit 1
[ ! -x "$(command -v cmake)" ] && printf 'Error: we need cmake!!\n' && exit 1
[ ! -x "$(command -v make)" ] && printf 'Error: we need make!!\n' && exit 1
[ ! -x "$(command -v gcc)" ] && printf 'Error: we need a toolchain: i.e. build-essential\n' && exit 1


cd "${WORK_DIR}"

# Cloning git
printf 'Actually doing the compilation stuff\n'
rm -rf "${WORK_DIR}/neovim"
git clone git@github.com:neovim/neovim.git "${WORK_DIR}/neovim" && cd "${WORK_DIR}/neovim" 
git ckeckout "${NVCHAD_GOOD_VER}"
make CMAKE_BUILD_TYPE=RelWithDebInfo CMAKE_INSTALL_PREFIX="${PREFIX}"
# installs everything to /usr/local/
make install

cd "${CWD}" # Returning to the current directory...
printf 'Deleting the work directory..\n'
rm -rf "${WORK_DIR}"

printf '\n\n'
printf 'Neovim Installed!!\n'
printf '\n\n'

