#+TITLE: Sentaurus Installation Guide for SCIPP
#+AUTHOR: Taylor Shin
#+STARTUP: showeverything
#+PROPERTY: header-args :tangle-mode
#+OPTIONS: tags: nil creator: nil tasks: nil todo: t
#+OPTIONS: H: 3 toc: 2
#+OPTIONS: html-preamble:t html-scripts:t html-style:t
#+OPTIONS: html5-fancy:nil text:t

* Sentaurus Installatio Guide for SCIPP
A short installation guide for Sentaurus 2015 for remote workers. You need at least a semi-modern distribution such as Ubuntu 18.04 and a [[https://its.ucsc.edu/mfa/quick-guide-campusvpn.html][UCSC Campus VPN]] activated to access the licensing server. Also, the guide is based on Ubuntu 20.04 based distributions on a virtual machine.

* Prerequisites
1. Ubuntu based Linux distribution, preferably 20.04. Must have GTK/Xorg based desktop environment. - Try to avoid Wayland desktop environment when logging in at GDM or LightDM.
2. VPN Connection to the Campus.
3. At least 40 GB of disk storage space. (Especially for Virtual Machines.)
4. (Virtual Machine) At least 2 CPUs and 4 GB of RAM.
5. Basic familiarity with Linux command line.

* (VM Only) Install Ubuntu 20.04.
You can download Ubuntu 20.04 LTS from [[https://releases.ubuntu.com/20.04.2.0/ubuntu-20.04.2.0-desktop-amd64.iso][here]].

Oracle [[https://www.virtualbox.org/][VirtualBox]] is a recommended virtualization software but take whatever you want. Note that some corporate machines do not allow to run virtual machines by turning of CPU's virtualization functionality such as Intel VT-x or AMD-V. You need to contact IT managers if it happens on a corporate machine. A lot of PC manufacturers (or motherboard AIB suppliers) ship their product with those virtualization functionality turned off. You can turn them on by accessing BIOS menu.

If your host machine is on VPN, we do not have to install VPN on the virtual machine. But it can be different from VM settings anyway. If the program complains on licensing, install VPN on the virtual machine as well. On the other hand, host machine can stay out of the VPN while the VM stays in the university network if you run the VPN client in the VM.

In fact, you can also install [[https://releases.ubuntu.com/22.04/ubuntu-22.04-desktop-amd64.iso][Ubuntu 22.04 LTS]] instead of 20.04 LTS.

* Install prerequisite packages
Some font packages and graphics libraries are required since the Sentaurus we have uses a bit old GUI library. You can install them via,

#+begin_src shell
sudo apt update -y && \
    sudo apt install -y \
    tcsh build-essential \
    mesa-common-dev xfonts-base xfonts-100dpi xfonts-75dpi \
    xvfb
#+end_src

On the other hand.. if your distribution is based on Red Hat or Fedora...

#+begin_src shell
sudo dnf -y update && \
    sudo dnf install -y \
    tcsh gcc-c++ \
    mesa-libGL-devel.x86_64mmesa-libglapi.x86_64 \
    mesa-libglapi.x86_64 mesa-libGLU.x86_64 \
    xorg-x11-fonts* \
    Xvfb
#+end_src

* Download Sentaurus Installation File from GoogleDrive
Ask Taylor Shin (tashin@ucsc.edu) to get access for Synopsys installer. Then go to the shared Google Drive Shared directory link to download =Synopsys_Archieve.tar.xz=.

You can unpack the file via,
#+begin_src shell
tar xf ./Synopsys_Archieve.tar.xz
#+end_src

Then move the unpacked =Synopsys_Archieve= directory somewhere secure such as =/opt=.

#+begin_src shell
sudo mv ./Synopsys_Archieve /opt/
#+end_src

* Set up Group policy for Synopsys Directory
The installation destination will be =/opt/Synopsys= directory. Sentaurus is capable of using user's directory as a work dir. However, this involves everyone setting up =STDB= environment variable set up and attaching the directory every time we run SWB. So, we will make a group in the system to let TCAD users have easier access to the Synopsys packages themselves. Thus, let us make a group called =tcad= and add all the TCAD users in the system.

#+begin_src shell
sudo groupadd tcad
sudo usermod -aG tcad <username>
#+end_src

And make =/opt/Synopsys/= directory for those users writable.

#+begin_src shell
sudo mkdir -p /opt/Synopsys
sudo chown -R :tcad /opt/Synopsys
sudo chmod -R 2775 /opt/Synopsys
#+end_src

Ok, once you or other users who are just added to the =tcad= group logs in again, the =/opt/Synopsys= directory will be writable. The group policy does not apply automatically. So every user added to =tcad= group must log and log in again!

* Install Synopsys Package
Let's move into the extracted directory to run =SynopsysInstaller_v3.3.run= as root.
#+begin_src shell
chmod +x ./SynopsysInstaller_v3.3.run
sudo ./SynopsysInstaller_v3.3.run
#+end_src

The install script will run. Make sure the installation directory somewhere, not the same as =Synopsys_Archieve= you are in. The default value is =[.]= which indicates current directory. In this case, let's assume =./Synopsys_Installer= directory which will be =/opt/Synopsys_Archieve/Synopsys_Installer=.

As long as you followed the previous section and =logged in again=, the permission problem will not exist! Let's run the installer!

#+begin_src shell
cd ./Synopsys_Installer
./install_now
#+end_src
Note that those installation scripts are written with =tcsh=.

Answer the installer questions with...

1. Site ID number: 27000
2. Site administrator: <blank is fine>
3. Site contact information: <blank is fine>
4. Path to the source directory: =/opt/Synopsys_Archieve=

If you answered all the questions correctly, the installer will ask versions. Select =2= for =sentaurus= then =1= to continue.

Then, say =yes= to install common file for product sentaurus and say =1= for =amd64= architecture.

Lastly, it will ask the directory the Sentaurus will reside. We have already prepared it for =/opt/Synopsys=. So, answer with:

#+begin_src
/opt/Synopsys/K_2015.06-SP2
#+end_src

Then, say =yes= to create directory and =yes= to accept the license to install. It will take a few minutes to extract all the files to fill up 3.1 GB of disk space.

If the installer says =done=, press Enter to quit the installer.


* Minor Tweak for Ubuntu 20.04 or any other modern Linux distributions
The Synopsys package we have originated in 2015 and Ubuntu 20.04 now has much advanced =libstdc++= library. Good thing is we can use Ubuntu 20.04's =libstdc++= to run any pre-compiled programs in the Sentaurus release 2015 but it needs some correct pointer. If you skip this part on some other distribution, a few visualization tools (such as SVisual and Structure Editor) and device design interface will not work or show black screen due to library mismatch.

To avoid the mayhem, we can just link system's =libstdc++.so= into the Sentaurus' directory.

#+begin_src shell
cd /opt/Synopsys/K_2015.06-SP2/tcad/K-2015.06-SP2/amd64/lib/
mv ./libstdc++.so.6 ./libstdc++.so.6.bak
ln -sfv /usr/lib/gcc/x86_64-linux-gnu/9/libstdc++.so \
    /opt/Synopsys/K_2015.06-SP2/tcad/K-2015.06-SP2/amd64/lib/libstdc++.so.6
#+end_src

The correct system library path can be backtracked with system GCC. To obtain the machine information, issue the command below.

#+begin_src shell
/usr/bin/gcc -dumpmachine
#+end_src

In most Ubuntu based distribution, it will say =x86_64-linux-gnu=. But some other machines, such as RHEL family, it will say =x86_64-redhat-linux=. In fact, in RHEL based distributions, =/usr/lib64= has corresponding =libstdc++.so.6= library. It differs from distribution to distribution. So be sure to takes some footwork. I recommend searching the =libstdc++.so=. Running a command like below will do the trick in most cases.

#+begin_src shell
find /usr/ -name libstdc++6.so
#+end_src

* Setting up Environment Variables
Lastly, you need to set up some environment variables. Add following lines in your =~/.bashrc= file then re-run the terminal emulator or issue command =. ~/.bashrc= to apply the change.

#+begin_src shell :tangle ./Synopsys_env.sh
SYNOPSYS_DIR="/opt/Synopsys"
SYNOPSYS_LICENSE='27000@license.soe.ucsc.edu'
SYNOPSYS_VER='K_2015.06-SP2'

if [ -d "$SYNOPSYS_DIR" ]; then
  # Note that STDB varialbe can be your own
  # Sentaurus project directory,
  # located anywhere you like.
  export STDB="$SYNOPSYS_DIR/DB"
  export LM_LICENSE_FILE="$SYNOPSYS_LICENSE"
  export PATH="$SYNOPSYS_DIR/$SYNOPSYS_VER/bin:$PATH"
  export STROOT="$SYNOPSYS_DIR/$SYNOPSYS_VER/"
  export STRELEASE=current
fi
#+end_src

** Run the Sentaurus Workbench (SWB) for the first time
We need to make the main workspace directory. The default is =/opt/Synopsys/DB= as you can read from the environment variable above.

#+begin_src shell
cd /opt/Synopsys/
mkdir DB
swb &
#+end_src

Then, you will assign =/opt/Synopsys/DB= directory when asked by the SWB which will be a main directory for all of your projects.

* Additional steps
** Install JEdit - We need to grab the plugin first!
As you've seen from SWB interface... the command editor... is not so comfortable to work with since we are more than 20 years away from the Y2K crisis. You can install [[http://www.jedit.org/][JEdit]] to make it much better experience. Ubuntu 20.04 has Jedit in the APT repository.

#+begin_src shell
sudo apt -y install jedit
#+end_src

You can also try to install jedit from jar file ([[https://sourceforge.net/projects/jedit/files/jedit/5.6.0/jedit5.6.0install.jar/download][Link]]) from JEdit home page. However, you might need newer JRE or JDK than the one shipped with your OS, most times. I suggest Azul Zulu Builds which can be found here: https://www.azul.com/downloads/

We need to install some plugin from Synopsys to use JEdit properly. The plugin installation requires SolveNet ID. So, we can ask some representitive who has it. You can access the download from: https://solvnet.synopsys.com/retrieve/1917300.html

** XTerm theme
The default terminal emulator SWB calls is the fossil old =xterm=. But we can use it as pretty as Ubuntu's default terminal. I recommend [[https://gitlab.com/dwt1/dotfiles/-/raw/master/.Xresources][Derek Taylor's .Xresources]] setting.

** Make a Desktop Icon for Sentaurus Workbench
Simply make =Sentaurus Workbench.desktop= file in =~/Desktop/= directory or =~/.local/share/applications/= directory with the contents below.
#+begin_src desktop
[Desktop Entry]
Version=1.0
Type=Application
Name=Sentaurus Workbench
Comment=
Exec=/opt/Synopsys/K_2015.06-SP2/bin/swb
Terminal=false
StartupNotify=false
#+end_src
