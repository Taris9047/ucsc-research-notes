# Raspberry Pi Basic Setup

## Overview
Raspberry Pi is an ARM based single board computer with GPIO capability. The CPU itself is usually Broadcom BCM2711 or similar series ARM microprocessors. So, any ARM based OS would work out but most easy to adopt is the [Raspbian OS](https://www.raspberrypi.com/software/). We will assume you have a SD card, with 32 GB of capacity at least, baked with Raspbian OS via Raspberry Pi Imager: [Windows](https://downloads.raspberrypi.org/imager/imager_latest.exe), [macOS](https://downloads.raspberrypi.org/imager/imager_latest.dmg), and [Ubuntu](https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb) platform. 

If you have Raspberry Pi 4 or newer, installing 64 bit is recommended. But 32 bit OS would also work out unless you are trying to adopt Raspberry Pi 5. We will start from enabling SSH, WiFi, and setting up hostname for Raspberry Pi5. In fact, modern Raspberry Pi Imager provides GUI set up method as well.

## Modern (and more preferred) Way
When you bake your SD Card with the Raspberry Pi Imager, you can select some variaties of OS after selecting Raspberry Pi Device. Then select the SD card from the Device button. Then an option pops up like below:
![RPi Imager Pop Up](./imgs/rp_imager_customization.png)

### Network (Including WiFi)
Obviously, we will 'Edit Settings' to see a window like this:
![RPi Imager General](./imgs/rp_imager_custom_general.png)
Mainly, you can set up hostname, user account, WiFi access, and timezone/locale here. If you set up hostname, you can connect the Raspberry Pi if it is under the same network via this `hostname.local` address. Say, `ssh jdoe@rasppi.local` connection is possible without knowing Raspberry Pi's IP address.

The wireless LAN menu can set up Raspberry Pi's default WiFi connection which would be mainly your home's WiFi router's address or laboratory's WiFi network which must be sharing Ethernet access with lab machines. Lastly, the time zone and keyboard layouts are obvious enough.

### SSH
In the next tab, Services, we can enable SSH server.
![RPi Imager Services](./imgs/rp_imager_custom_services.png)
Here, you can enable SSH server for Raspberry Pi. By default and ease of explanation, we will stay as password authentication. 

But you can also set it up as a public key authentication method. In this method, no one can connect to SSH server without a SSH public key assigned to this Raspberry Pi's user, which you saw at the page right before this. This is a secure method of access to prevent hacking attempt with password input. But for Raspberry Pi set up for microelectronics applications, this kind of authentication may not be necessary. To figure out what this method is, please refer [this](https://www.youtube.com/watch?v=RfolgB-rVe8) lecture instead or ask IT Specialist at SCIPP.

Once you set up everything, hit `Save` button to continue baking the SD card with Raspberry Pi's OS. Then you will be able to connect to the Raspberry Pi via the SSH as long as the Pi is under the same network as your host PC. Yes, you do not need keyboard or mouse, nor monitor!! This is called the **Headless** method.


## Primitive Way (May not work with modern RPi: 4 or above)
### SSH
Put in the SD card baked with Raspbian OS into a PC, then navigate into `/boot/` directory. Then make a simple text file named `ssh`. 

#### Windows Caveat
Make sure you have not added any extension. Windows, by the way, does not show file extension by default. If you just make a new text file in any directory, the filename becomes actually, `<filename>.txt`. So, the actual filename becomes `something.txt` which is not what we want this case. Make sure you turn off `Hide extensions for known file types` from `Windows Explorer --> 3 dots (Windows 11) --> Options --> View tab`. This hiding file type fiasco started since Windows 95 days and still lingers on to make people confused.

### WiFi
On an Old Raspbian OS (legacy) or on Lite version, WiFi can be set up with a text file located at `wpa_supplicant.conf` in `/boot` or `/etc/wpa_supplicant` directory (possibly the latter for Raspberry Pi 3 or 3+). But this is not the case anymore and it is actually recommended to use the Raspberry Pi Imager interface. 

The old way, an example `wpa_supplicant.conf`:
```
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
  scan_ssid=1
  ssid="your_wifi_ssid"
  psk="your_wifi_password"
}
```

If WiFi must be set up post install, at least enable SSH from the Raspberry Pi Imager, then prepare a male-to-male RJ-45 (or just an Ethernet) cable to connect the Pi to the host PC if it has RJ-45 connector slot. 

RJ-45 Cable:
![RJ45cable](./imgs/rj_45_cable.png)

RJ-45 Connector on a Host PC:
![RJ45onHost](./imgs/rj45_laptop_host.png)

You will have to purchase any USB-to-RJ45 adapter (actually they are USB LAN cards) to be able to use this method.

Once you connect Raspberry Pi and the Host PC with a RJ-45 cable, you will find some strange connection in your PC's network settings screen saying some 'Unidentified network' or so. Then you will need to start some digging into the subnet. Please refer [this](https://rimstar.org/science_electronics_projects/connect_to_raspberry_pi_via_ethernet_directly.htm#:~:text=Plug%20one%20end%20into%20the,power%20to%20your%20Raspberry%20Pi.) page for details.

## The Most Important (VNC)
Once you successfully log into Raspberry Pi via SSH, you will need to issue an administrator privilege command:
```
sudo raspi-config
```
Then you will see a configuration menu like this:
![raspi-config-main](./imgs/raspi-config-menu.png)

Go to `Interface Options --> VNC`

Then enable VNC server. Unlike the other VNC guide, you do not have to run VNC server every reboot on the Raspberry Pi. As long as the Raspberry Pi runs, you can connect to VNC server of Raspberry Pi via the hostname or IP address of the Raspberry Pi with [RealVNC Viewer](https://www.realvnc.com/en/connect/download/viewer/raspberrypi/). It will ask your Raspberry Pi's user account credentials to connect. 

In fact, Raspbian OS comes with an official RealVNC Server for free. It does not support cloud based VNC service but way more convenient than TightVNC or similar 'free' VNC servers. But then again, this VNC server supports copy and paste between host and VNC.

## Update Raspberry Pi
Since most likely this walkthrough is your first, updating the Raspbian OS is recommended to start anything with it. It can be done with GUI: You will find an update button on the top status bar if any new updates are found.

or you can issue command in the Raspberry Pi's console:
```
sudo apt update && sudo apt -y upgrade
```

## Old Language Support (C/C++)
Raspbian OS ships with a default Python implementation with basic GPIO libraries. But Python interpretation is not very speedy and prevents user to access advanced features like CPU direct GPIO interfacing or DMA. In short, you need C or C++ to directly control the Broadcom Microcontroller to actually use Raspberry Pi professionally. 

### Install GNU Compilers
To set up any development environment, you will need to understand `apt` or `apt-get` package management system which is quite widely adopted by Linux community due to them being a default package manager of Debian and Ubuntu based Linux distributions.

To install the compilers, simply type command in Raspberry Pi (either via SSH connection or VNC GUI access):
```
sudo apt install -y build-essential
```
If you are on Lite or Legacy Raspbian, use `apt-get` rather than `apt` command. The `build-essential` installs C and C++ compilers with some build tools including `gnumake` and `autoconf` with `m4` libraries. Of course, you will find a debugger, `gdb`, is installed with this command as well.

For some reason, if you need other compilers than GNU supplied ones, you can also install them just like any Debian or Ubuntu based Linux distributions like this:
```
sudo apt install -y clang
```

## Editors
Obviously, you can use many editors in the Raspberry Pi since the OS is based on Linux. By default, you can use `nano`, `vim`, and even `ed`. But they are mostly command line based editors and not very easy to work with if you aren't experienced with those 'old way.'

So I would recommend a few more editors which are available through Raspbian OS' default program list. You can browse the software list from GUI screen. `Raspberry Button --> Preference --> Recommended Software`.

1. Mu
    ![Mu editor](./imgs/mu_editor.png)
    Is a simple Python editor that also provides python syntax check and code formatting for microcontroller programming. Also provides some debugging and REPL (Python console) access. The Plotter is a simple numeric to linear plotting feature that reads in RS232 serial interface which is a common debug method for microcontroller (including Arduino) programming. 

    But this editor is not equipped for anything complex with 2 or more Python scripts.  
  
  
2. Geany
    ![Geany IDE](./imgs/geany.png)
    Geany is more of a 'General' purpose IDE that can handle a moderately complex projects written in C/C++ with Makefile build system. You can also work with Python without problem as well. Geany is available on any x86 based Linux distributions but there are so many more advanced (but heavier, meaning requires more memory and CPU power) editors such as Visual Studio Code, etc.

    But since Raspberry Pi's computing resource is obviously limited, Geany can be a great alternative for Raspberry Pi development. Geany interacts with Makefile build system and gdb debugger.