EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Diamond Sensor Test"
Date "2021-05-03"
Rev ""
Comp "UCSC - SCIPP"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:0 #GND01
U 1 1 6089404F
P 2800 5200
F 0 "#GND01" H 2800 5100 50  0001 C CNN
F 1 "0" H 2800 5289 50  0000 C CNN
F 2 "" H 2800 5200 50  0001 C CNN
F 3 "~" H 2800 5200 50  0001 C CNN
	1    2800 5200
	1    0    0    -1  
$EndComp
$Comp
L pspice:C C1
U 1 1 608960E0
P 7450 4150
F 0 "C1" H 7628 4196 50  0000 L CNN
F 1 "4pF" H 7628 4105 50  0000 L CNN
F 2 "" H 7450 4150 50  0001 C CNN
F 3 "~" H 7450 4150 50  0001 C CNN
	1    7450 4150
	1    0    0    -1  
$EndComp
$Comp
L Simulation_SPICE:VDC V1
U 1 1 60897074
P 7450 4800
F 0 "V1" H 7580 4891 50  0000 L CNN
F 1 "100V" H 7580 4800 50  0000 L CNN
F 2 "" H 7450 4800 50  0001 C CNN
F 3 "~" H 7450 4800 50  0001 C CNN
F 4 "Y" H 7450 4800 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 7450 4800 50  0001 L CNN "Spice_Primitive"
F 6 "dc(1)" H 7580 4709 50  0000 L CNN "Spice_Model"
	1    7450 4800
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND03
U 1 1 60899622
P 7450 5250
F 0 "#GND03" H 7450 5150 50  0001 C CNN
F 1 "0" H 7450 5339 50  0000 C CNN
F 2 "" H 7450 5250 50  0001 C CNN
F 3 "~" H 7450 5250 50  0001 C CNN
	1    7450 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 5250 7450 5000
$Comp
L Device:R R5
U 1 1 6089B258
P 3800 3200
F 0 "R5" V 3593 3200 50  0000 C CNN
F 1 "1" V 3684 3200 50  0000 C CNN
F 2 "" V 3730 3200 50  0001 C CNN
F 3 "~" H 3800 3200 50  0001 C CNN
	1    3800 3200
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 6089D6F0
P 3250 3200
F 0 "R2" V 3043 3200 50  0000 C CNN
F 1 "1m" V 3134 3200 50  0000 C CNN
F 2 "" V 3180 3200 50  0001 C CNN
F 3 "~" H 3250 3200 50  0001 C CNN
	1    3250 3200
	0    1    1    0   
$EndComp
$Comp
L Device:L L1
U 1 1 6089FE8A
P 5050 3200
F 0 "L1" V 5240 3200 50  0000 C CNN
F 1 "10p" V 5149 3200 50  0000 C CNN
F 2 "" H 5050 3200 50  0001 C CNN
F 3 "~" H 5050 3200 50  0001 C CNN
	1    5050 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3650 3200 3500 3200
Wire Wire Line
	3100 3200 2800 3200
Wire Wire Line
	7450 4600 7450 4450
$Comp
L Device:R R8
U 1 1 608AFF48
P 7100 3200
F 0 "R8" V 6893 3200 50  0000 C CNN
F 1 "4" V 6984 3200 50  0000 C CNN
F 2 "" V 7030 3200 50  0001 C CNN
F 3 "~" H 7100 3200 50  0001 C CNN
	1    7100 3200
	0    1    1    0   
$EndComp
$Comp
L Simulation_SPICE:IPULSE I1
U 1 1 608B4531
P 7450 3600
F 0 "I1" H 7580 3691 50  0000 L CNN
F 1 "IPULSE" H 7580 3600 50  0000 L CNN
F 2 "" H 7450 3600 50  0001 C CNN
F 3 "~" H 7450 3600 50  0001 C CNN
F 4 "Y" H 7450 3600 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "I" H 7450 3600 50  0001 L CNN "Spice_Primitive"
F 6 "pulse(0 -1e12 1p 0.01p 0.01p 2p 50p)" H 7580 3509 50  0000 L CNN "Spice_Model"
	1    7450 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3200 7450 3400
Wire Wire Line
	7450 3900 7450 3850
$Comp
L Device:R R7
U 1 1 608B5AEE
P 6800 4150
F 0 "R7" H 6870 4196 50  0000 L CNN
F 1 "10MEG" H 6870 4105 50  0000 L CNN
F 2 "" V 6730 4150 50  0001 C CNN
F 3 "~" H 6800 4150 50  0001 C CNN
	1    6800 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3850 6800 3850
Wire Wire Line
	6800 3850 6800 4000
Connection ~ 7450 3850
Wire Wire Line
	7450 3850 7450 3800
Wire Wire Line
	6800 4300 6800 4450
Wire Wire Line
	6800 4450 7450 4450
Connection ~ 7450 4450
Wire Wire Line
	7450 4450 7450 4400
$Comp
L Device:R R3
U 1 1 608BA941
P 3500 3900
F 0 "R3" H 3570 3946 50  0000 L CNN
F 1 "5" H 3570 3855 50  0000 L CNN
F 2 "" V 3430 3900 50  0001 C CNN
F 3 "~" H 3500 3900 50  0001 C CNN
	1    3500 3900
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND02
U 1 1 608BB2A8
P 3500 5200
F 0 "#GND02" H 3500 5100 50  0001 C CNN
F 1 "0" H 3500 5289 50  0000 C CNN
F 2 "" H 3500 5200 50  0001 C CNN
F 3 "~" H 3500 5200 50  0001 C CNN
	1    3500 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3200 3500 3750
Connection ~ 3500 3200
Wire Wire Line
	3500 3200 3400 3200
Wire Wire Line
	7450 3200 7250 3200
$Comp
L Device:R R4
U 1 1 60899FFB
P 3500 4650
F 0 "R4" H 3570 4696 50  0000 L CNN
F 1 "50" H 3570 4605 50  0000 L CNN
F 2 "" V 3430 4650 50  0001 C CNN
F 3 "~" H 3500 4650 50  0001 C CNN
	1    3500 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5200 3500 4800
Wire Wire Line
	2800 5200 2800 4800
Wire Wire Line
	3500 4500 3500 4300
$Comp
L Device:R R1
U 1 1 6089E305
P 2800 4650
F 0 "R1" H 2870 4696 50  0000 L CNN
F 1 "50" H 2870 4605 50  0000 L CNN
F 2 "" V 2730 4650 50  0001 C CNN
F 3 "~" H 2800 4650 50  0001 C CNN
	1    2800 4650
	1    0    0    -1  
$EndComp
Text GLabel 3350 4300 0    50   Input ~ 0
Out1Ohm
Text GLabel 2650 4300 0    50   Input ~ 0
Out1mOhm
Wire Wire Line
	3350 4300 3500 4300
Wire Wire Line
	2650 4300 2800 4300
Wire Wire Line
	2800 4300 2800 4500
Text GLabel 6250 3050 1    50   Input ~ 0
Sensor_Output
Wire Wire Line
	3950 3200 4500 3200
Text GLabel 4500 3000 1    50   Input ~ 0
IndiumBond
Wire Wire Line
	4500 3200 4500 3000
Wire Wire Line
	4500 3200 4800 3200
Connection ~ 4500 3200
Wire Notes Line
	9300 4600 9300 2700
Wire Notes Line
	4300 4400 6400 4400
Wire Notes Line
	2550 4450 2550 5300
Wire Notes Line
	2550 5300 3800 5300
Wire Notes Line
	3800 5300 3800 4450
Wire Notes Line
	3800 4450 2550 4450
Wire Notes Line
	6400 2400 4300 2400
$Comp
L Device:C C2
U 1 1 608B1001
P 4800 3600
F 0 "C2" H 4915 3646 50  0000 L CNN
F 1 "4.427e-14" H 4915 3555 50  0000 L CNN
F 2 "" H 4838 3450 50  0001 C CNN
F 3 "~" H 4800 3600 50  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 608B1ED1
P 5800 3600
F 0 "C3" H 5915 3646 50  0000 L CNN
F 1 "4.427e-14" H 5915 3555 50  0000 L CNN
F 2 "" H 5838 3450 50  0001 C CNN
F 3 "~" H 5800 3600 50  0001 C CNN
	1    5800 3600
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND05
U 1 1 608B24CF
P 5800 4000
F 0 "#GND05" H 5800 3900 50  0001 C CNN
F 1 "0" H 5800 4089 50  0000 C CNN
F 2 "" H 5800 4000 50  0001 C CNN
F 3 "~" H 5800 4000 50  0001 C CNN
	1    5800 4000
	1    0    0    -1  
$EndComp
$Comp
L pspice:0 #GND04
U 1 1 608B31F5
P 4800 4000
F 0 "#GND04" H 4800 3900 50  0001 C CNN
F 1 "0" H 4800 4089 50  0000 C CNN
F 2 "" H 4800 4000 50  0001 C CNN
F 3 "~" H 4800 4000 50  0001 C CNN
	1    4800 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4000 5800 3750
Wire Wire Line
	5800 3450 5800 3200
Wire Wire Line
	4800 4000 4800 3750
Wire Wire Line
	4800 3450 4800 3200
Wire Notes Line
	4300 2400 4300 4400
$Comp
L Device:R R6
U 1 1 608B8ECA
P 5500 3200
F 0 "R6" V 5293 3200 50  0000 C CNN
F 1 "1" V 5384 3200 50  0000 C CNN
F 2 "" V 5430 3200 50  0001 C CNN
F 3 "~" H 5500 3200 50  0001 C CNN
	1    5500 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 3200 5800 3200
Wire Wire Line
	5350 3200 5200 3200
Wire Wire Line
	4900 3200 4800 3200
Connection ~ 4800 3200
Text Notes 2800 5400 0    50   ~ 0
Readout Apparatus
Text Notes 4950 4500 0    50   ~ 0
The Indium Plate
Text Notes 7750 2700 0    50   ~ 0
Sensor
Text Notes 4900 4250 0    50   ~ 0
Equivalent Capacitors:\nAssuming \n 1-mm-thick\n 10-mm-wide\n 5-mm-long strip.\nAir as diaelectric.
Text Notes 2550 5600 0    50   ~ 0
Apparently, we would not feed directly into those apparatus.\nThis is just 50 Ohm input impedance emulation.
Text Notes 3550 4150 0    50   ~ 0
Jumper\n
Text Notes 7600 3850 0    50   ~ 0
IPULSE emulates the photo-current.
Text Notes 7550 4550 0    50   ~ 0
10MEG Ohm emulates intrinsic resistance\nof the sensor.
Connection ~ 5800 3200
Wire Notes Line
	6400 2400 6400 4400
Wire Wire Line
	5800 3200 6250 3200
Connection ~ 6250 3200
Wire Wire Line
	6250 3200 6950 3200
Wire Wire Line
	6250 3050 6250 3200
Wire Notes Line
	3400 3700 3400 4050
Wire Notes Line
	3400 4050 3700 4050
Wire Notes Line
	3700 4050 3700 3700
Wire Notes Line
	3700 3700 3400 3700
Text Notes 7000 7000 0    50   ~ 0
Git repository:\nhttps://gitlab.com/Taris9047/ucsc-research-notes/-/tree/master/Diamond_Test/Diamond_Test
Connection ~ 2800 4300
Connection ~ 3500 4300
Wire Wire Line
	3500 4300 3500 4050
Wire Wire Line
	2800 3200 2800 4300
Wire Notes Line
	6650 2700 9300 2700
Wire Notes Line
	9300 4600 6650 4600
Wire Notes Line
	6650 2700 6650 4600
$EndSCHEMATC
