#!/usr/bin/env python3

import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class PlotHorizontalCSV(object):
    """
    Actually plots with the output csv file.
    """
    def __init__(self, csv_file, 
        outf_format='png', 
        outf_name='', 
        xlabel='', 
        ylabel='', 
        show=False):
        
        if outf_format:
            self.outf_format = outf_format
        else:
            raise ValueError("Output image format is invalid!!")
        
        if os.path.exists(csv_file):
            self.csv_file = csv_file
            csv_basename = os.path.splitext(os.path.basename(self.csv_file))[0]
        else:
            raise ValueError("Invalid input csv file!!")
        
        self.ylabel = ylabel
        self.xlabel = xlabel
        self.ShowPlt = show
        
        if outf_name == '':
            self.outf_name = '.'.join([csv_basename, outf_format])
        else:
            self.outf_name = '.'.join([outf_name, outf_format])
        
        csv_data = pd.read_csv(self.csv_file, sep=';', header=None)
        print("Reading {}".format(csv_file))
        data = csv_data.drop(columns=csv_data.columns[-1]).values
        
        self.labels = []
        self.data_set = []
        for d_row in data:
            self.labels.append(d_row[0])
            self.data_set.append(d_row[1:])
        
        self.Plot()
    
    def Plot(self):
        if not self.xlabel:
            x_label = self.labels.pop(0)
        else:
            x_label = self.xlabel
            self.labels.pop(0)
        x_data = self.data_set.pop(0)
        
        plt.figure()
        for lbl, data in zip(self.labels, self.data_set):
            plt.plot(x_data, data, label=lbl)
        plt.xlabel(x_label)
        plt.ylabel(self.ylabel)
        plt.legend()
        plt.savefig(self.outf_name)
        print("Saved a figure for {} as {}".format(self.csv_file, self.outf_name))
        
        if self.ShowPlt:
            plt.show()
        
        plt.close()

class PlotTransient(object):
    """
    Plots Voltage and Current data from the SPICE simulator.
    """
    def __init__(self, csv_voltage, csv_current):
        plt_volt = PlotHorizontalCSV(
            os.path.realpath(csv_voltage), xlabel='Time (s)', ylabel='Voltages (V)')
        plt_curr = PlotHorizontalCSV(
            os.path.realpath(csv_current), xlabel='Time (s)', ylabel='Currents (A)')

if __name__ == "__main__":
    PlotTransient(
        './OutputVoltage.csv',
        './Current.csv'
    )